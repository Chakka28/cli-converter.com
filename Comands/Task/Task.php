<?php

/**
 * Clase Task
 */
class Task extends BaseTask
{
    /**
     * Initialize (Ejecutada de primera por el administrador de hilos)
     * 
     * @return mixed
     */
    public function initialize() 
    {
        return true;
    }

    /**
     * Ejecutada por el administrador de hilos si el proceso se completó con exito (Cuando el metodo process() haya retornado true)
     * 
     * @return mixed
     */
    public function onSuccess()
    {
        return true;
    }

    /**
     * Ejecutada por el administrador de hilos si el proceso se completó con fallos (Cuando el metodo process() haya retornado false)
     * 
     * @return mixed
     */
    public function onFailure() 
    {
        return false;
    }

    /**
     * Método principal que contiene la lógica a ser ejecutada por la tarea
     * 
     * @param $params array Array asociativo de parametros
     *
     * @return boolean True para Éxito, false de lo contrario
     */
    public function process(array $params = array())
    {
        $conexion = new conexion();
         $format = $params["format"];
	    $link = $params["link"];
	    $name = $params["name"];
        $comand = "sudo youtube-dl --recode-video $format -o '/var/www/html/downloads/$name'  $link";
        $resultado = exec($comand);
        $conexion->updateFile($name , "Finished");
        $url = "http://192.168.150.5/downloads/".$name.".".$format;
        $conexion->addDownloads($name, $url);
        return true;
    }
}
