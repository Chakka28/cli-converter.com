<?php
require 'vendor/autoload.php';
require ("Task/addBrokenLink.php");
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class receiveRabbit{

    public  $instanceConexion;
    public  $instanceBrokenLink;
   // public  $instancedownload;
    public function recive()
    { 
        $this->instanceConexion = new conexion();
        $this->instanceBrokenLink = new addBrokenLink(); 
       // $this->instancedownload = new download();

        $connection = new AMQPStreamConnection('192.168.40.10', 5672, 'admin', 'password');
        $channel = $connection->channel();
        

        $channel->queue_declare(
            $queue = 'converter',
            $passive = false,
            $durable = false,
            $exclusive = false,
            $auto_delete = false,
            $nowait = false,
            $arguments = null,
            $ticket = null
        );


        $callback = function($msg) {
            sleep(substr_count($msg->body, '.'));
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            $condition = json_decode($msg->body);
            
            if(empty($condition)){
                echo "no hay colas";
            }
            else{
                $this->instanceConexion->updateFile($condition->id, "Process");
                $array = [
                    "format" => $condition->format,
		            "link" => $condition->link,
		            "name" => $condition->id
                ];
                if($this->instanceBrokenLink->add($condition->link,$condition->id,$condition->format)){
                    echo $condition->link;
                    echo "Este es un enlace duro\n";
                }else{
                    $Task = new Task();
                    $multithreadManager = new ThreadManager();
                    $multithreadManager->start($Task, $array);
                    echo $condition->link;
                    echo "\n";
                }
                
            }
            
        };

        $channel->basic_qos(null,1, null);

        $channel->basic_consume('converter', '', false, false, false, false, $callback);
        

        while ($channel->is_consuming()) {
            if($this->instanceConexion->getSpaces() < 5){
                $channel->wait();
            }else{
            }
        }

        $channel->close();
        $connection->close();
    }

} 


?>
